# helloworld-kubernetes-operator

A dummy operator to check how an operator works

## Setup

Install the operator sdk:

    brew install operator-sdk
    
## Adding API and controller

Use

    operator-sdk add api --api-version=hello.world.api/v1alpha1 --kind=HelloWorldData
    operator-sdk add controller --api-version=hello.world.api/v1alpha1 --kind=HelloWorldData
    operator-sdk generate k8s

## Build it

Via

    operator-sdk build helloworld-operator
    
## Test it

Switch (via kubectl) to your test server. Usually minikube or your local docker kubernetes cluster!!!

Then:

    kubectl create -f deploy/crds/hello_v1alpha1_helloworlddata_crd.yaml
    export OPERATOR_NAME=helloworld-kubernetes-operator
    operator-sdk up local --namespace=default

Deploy your test pod(s):

    kubectl apply -f deploy/crds/hello_v1alpha1_helloworlddata_cr.yaml

Check your pods:

    kubectl get pods --namespace=default -o wide
    kubectl describe pod example-helloworlddata-pod

## Delete it

Simply:

    kubectl delete -f deploy/crds/hello_v1alpha1_helloworlddata_cr.yaml
    # kubectl delete -f deploy/

## Prepare for release

The manual suggests to use this method:

    operator-sdk olm-catalog gen-csv --csv-version 0.0.1
    