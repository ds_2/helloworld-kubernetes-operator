package controller

import (
	"gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/controller/helloworlddata"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, helloworlddata.Add)
}
