// Package v1alpha1 contains API Schema definitions for the hello v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=hello.world.api
package v1alpha1
