// +build !ignore_autogenerated

// This file was autogenerated by openapi-gen. Do not edit it manually!

package v1alpha1

import (
	spec "github.com/go-openapi/spec"
	common "k8s.io/kube-openapi/pkg/common"
)

func GetOpenAPIDefinitions(ref common.ReferenceCallback) map[string]common.OpenAPIDefinition {
	return map[string]common.OpenAPIDefinition{
		"gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldData":       schema_pkg_apis_hello_v1alpha1_HelloWorldData(ref),
		"gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataSpec":   schema_pkg_apis_hello_v1alpha1_HelloWorldDataSpec(ref),
		"gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataStatus": schema_pkg_apis_hello_v1alpha1_HelloWorldDataStatus(ref),
	}
}

func schema_pkg_apis_hello_v1alpha1_HelloWorldData(ref common.ReferenceCallback) common.OpenAPIDefinition {
	return common.OpenAPIDefinition{
		Schema: spec.Schema{
			SchemaProps: spec.SchemaProps{
				Description: "HelloWorldData is the Schema for the helloworlddata API",
				Properties: map[string]spec.Schema{
					"kind": {
						SchemaProps: spec.SchemaProps{
							Description: "Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds",
							Type:        []string{"string"},
							Format:      "",
						},
					},
					"apiVersion": {
						SchemaProps: spec.SchemaProps{
							Description: "APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources",
							Type:        []string{"string"},
							Format:      "",
						},
					},
					"metadata": {
						SchemaProps: spec.SchemaProps{
							Ref: ref("k8s.io/apimachinery/pkg/apis/meta/v1.ObjectMeta"),
						},
					},
					"spec": {
						SchemaProps: spec.SchemaProps{
							Ref: ref("gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataSpec"),
						},
					},
					"status": {
						SchemaProps: spec.SchemaProps{
							Ref: ref("gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataStatus"),
						},
					},
				},
			},
		},
		Dependencies: []string{
			"gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataSpec", "gitlab.com/ds_2/helloworld-kubernetes-operator/pkg/apis/hello/v1alpha1.HelloWorldDataStatus", "k8s.io/apimachinery/pkg/apis/meta/v1.ObjectMeta"},
	}
}

func schema_pkg_apis_hello_v1alpha1_HelloWorldDataSpec(ref common.ReferenceCallback) common.OpenAPIDefinition {
	return common.OpenAPIDefinition{
		Schema: spec.Schema{
			SchemaProps: spec.SchemaProps{
				Description: "HelloWorldDataSpec defines the desired state of HelloWorldData",
				Properties:  map[string]spec.Schema{},
			},
		},
		Dependencies: []string{},
	}
}

func schema_pkg_apis_hello_v1alpha1_HelloWorldDataStatus(ref common.ReferenceCallback) common.OpenAPIDefinition {
	return common.OpenAPIDefinition{
		Schema: spec.Schema{
			SchemaProps: spec.SchemaProps{
				Description: "HelloWorldDataStatus defines the observed state of HelloWorldData",
				Properties:  map[string]spec.Schema{},
			},
		},
		Dependencies: []string{},
	}
}
